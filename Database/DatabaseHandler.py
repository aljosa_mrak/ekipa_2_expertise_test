import sqlite3

import os

import config


def get_connection():
    # Check for specified database type
    if config.database_type == 'sqlite':
        # Create path to database
        if not os.path.exists(os.path.dirname(config.sqlite_file_name)):
            os.makedirs(os.path.dirname(config.sqlite_file_name))

        # Connect to database
        connection = sqlite3.connect(config.sqlite_file_name)
    else:
        print "Unknown or unimplemented database type '%s'" % config.database_type
        return

    # Create table if doesn't exist
    if create_table(connection, config.database_table_name):
        return connection
    else:
        return None


def create_table(connection, table_name):
    if connection is None or table_name is None or len(table_name) == 0:
        return False

    cursor = connection.cursor()

    # Create table
    sql = """CREATE TABLE IF NOT EXISTS {0} (
             id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
             date  DATE,
             adNetwork  CHAR(20),
             app CHAR(20),
             platform CHAR(20),
             requests INTEGER,
             impressions INTEGER,
             revenue DECIMAL(18, 2))""".format(table_name)

    try:
        cursor.execute(sql)
    except sqlite3.OperationalError as e:
        print e
        connection.rollback()
        cursor.close()
        return False

    connection.commit()
    cursor.close()

    return True
