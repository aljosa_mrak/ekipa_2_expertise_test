import pandas as pd
from currency_converter import CurrencyConverter

from AdNetworks.AbstractAdNetwork import *
from helper.cyton import parse_revenue_umbrella_network


class UmbrellaNetwork(AbstractAdNetwork):
    name = 'AdUmbrella'
    url_pattern = 'https://storage.googleapis.com/expertise-test/reporting/adumbrella/adumbrella-%d_%d_%d.csv'
    file_name_pattern = 'data/adumbrella-%d_%d_%d.csv'

    def __init__(self):
        self.currency_converter = CurrencyConverter()

    def get_url(self, day, month, year):
        return self.url_pattern % (day, month, year)

    def get_file_name(self, day, month, year):
        return self.file_name_pattern % (day, month, year)

    def read_csv(self, source):
        data = pd.read_csv(source,
                           sep=",",
                           header=0,
                           skipfooter=1,
                           names=COLUMN_NAMES,
                           engine='python',
                           parse_dates=[DATE])
        data[REVENUE] = parse_revenue_umbrella_network(data[[DATE, REVENUE]].as_matrix(), self.currency_converter)
        return data
