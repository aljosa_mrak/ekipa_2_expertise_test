import os
import sys

import numpy as np
import pandas as pd
import scipy
from scipy.stats import multivariate_normal

from AdNetworks.AbstractAdNetwork import *
from AdNetworks.SuperNetwork import SuperNetwork
from AdNetworks.UmbrellaNetwork import UmbrellaNetwork

COLUMNS = ['date', 'adNetwork', 'app', 'platform', 'requests', 'impressions', 'revenue']
PLATFORMS = ['Android', 'iOS']
APPS = ['Talking Ginger', 'My Talking Angela', 'My Talking Ben', 'My Talking Tom']

NUMERIC_COLUMNS = [REQUESTS, IMPRESSIONS, REVENUE]

STATISTICS = [np.min, np.max, np.mean, np.std, np.median, np.sum]


def assert_print(condition, msg):
    if not condition:
        print msg


def read_data(_file):
    if _file.startswith("adumbrella"):
        obj = UmbrellaNetwork()
        obj.load_data('data/' + _file)
        return obj.df
    else:
        obj = SuperNetwork()
        obj.load_data('data/' + _file)
        return obj.df


if __name__ == "__main__":

    # Parse arguments
    if len(sys.argv) > 1:
        files = np.hstack([os.listdir(str(arg)) for arg in sys.argv[1:]])
    else:
        print 'No path specified. List all paths as arguments'
        sys.exit(1)

    statistical_data = np.zeros((len(files), len(NUMERIC_COLUMNS), len(STATISTICS)))

    # Read all specified files
    for i in range(len(files)):
        try:
            data = read_data(files[i])
            if data is None:
                print "Can not read file '%s'. Check if empty" % files[i]
                continue
        except Exception as e:
            print "Can not read file '%s'. Check if empty" % files[i]
            print e
            continue

        columns = data.columns

        # Check columns
        assert_print(len(columns) == len(COLUMNS), "Wrong number of columns in file '%s' " % files[i])
        assert_print(sorted(columns) == sorted(COLUMNS), "Wrong column names in file '%s' " % files[i])
        assert_print(np.all([not np.any(pd.isnull(data[c])) for c in columns]),
                     "Data contains Null in file '%s' " % files[i])

        # Check platform
        assert_print(len(data[PLATFORM].unique()) <= len(PLATFORMS),
                     "Too many unique platforms in file '%s' " % files[i])
        assert_print(np.all([p in PLATFORMS for p in data[PLATFORM]]), "Unknown platforms in file '%s' " % files[i])

        # Check app
        assert_print(len(data[APP].unique()) <= len(APPS), "Too many unique apps in file '%s' " % files[i])
        assert_print(np.all([p in APPS for p in data[APP]]), "Unknown apps in file '%s' " % files[i])

        # Check date
        assert_print(len(data[DATE].unique()) == 1, "Data contains more than one date in file '%s' " % files[i])

        # Calculate all statistics
        for col in range(len(NUMERIC_COLUMNS)):
            for stat in range(len(STATISTICS)):
                statistical_data[i, col, stat] = STATISTICS[stat](data[NUMERIC_COLUMNS[col]])

    # Compute average and std for each statistic and column. Average files together
    point_center, point_std = np.mean(statistical_data, axis=0), np.std(statistical_data, axis=0)

    # Compute z-score or number of sigmas for each and then compute median it though statistics and column
    #  to get one median for each file
    z_score = np.median((statistical_data - point_center) / point_std, axis=(1, 2))

    # Convert z-score to percentile
    percentile = scipy.stats.norm.cdf(z_score)

    # Print result
    for (per, i) in sorted(zip(percentile, range(len(percentile))), reverse=True):
        print "File '%s' does not look OK with probability of %.2f %%" % (files[i], per * 100)
