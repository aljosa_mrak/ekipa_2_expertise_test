#!/usr/bin/python
import argparse
from dateutil.parser import parse

import config
from AdNetworks import AdNetworkHandler
from Database.DatabaseHandler import get_connection
from helper import Utils


def main(arguments):
    # Get arguments
    if arguments is None or len(arguments) < 2 or arguments[0] is None or arguments[1] is None:
        print 'Invalid number of arguments'
        return
    ad_network = arguments[0]
    try:
        date = parse(arguments[1], yearfirst=True, dayfirst=True)
    except ValueError as e:
        print e
        return

    if len(arguments) > 2 and arguments[2] == True:
        from_file = True
    else:
        from_file = False

    # Get data
    network_report = AdNetworkHandler.get_report(ad_network, date.day, date.month, date.year, from_file=from_file)
    if network_report is None:
        return

    # Connect to database
    connection = get_connection()
    if connection is None:
        return

    # Write data to database
    network_report.df.to_sql(config.database_table_name, connection, index=False, if_exists='append')

    # Close connection
    connection.close()


if __name__ == '__main__':
    # Argument parsing
    parser = argparse.ArgumentParser(description='Get data from adNetwork and store it in database')
    parser.add_argument('-f', '--file', action='store_false', dest='from_file', help='flag to read data from file')
    parser.add_argument('-l', '--log', action='store_false', dest='log_file', help='flag to write logs in file')
    parser.add_argument('-v', '--verbose', action='store_false', help='report on what is done')
    parser.add_argument('ad_network', type=str, help='adNetwork name')
    parser.add_argument('date', type=str, help='date of the report')
    args = parser.parse_args()

    # Run main function through helper function to setup logging and timing
    Utils.run(main, (args.ad_network, args.date, args.from_file), to_file=args.log_file, verbose=args.verbose)
