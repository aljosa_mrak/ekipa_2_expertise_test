from unittest import TestCase

import collecting_data
import config
from AdNetworks import AdNetworkHandler
from Database import DatabaseHandler
from tests.test_DatabaseHandler import TestDatabaseHandler


def clear_data():
    connection = DatabaseHandler.get_connection()
    cursor = connection.cursor()
    cursor.execute("DROP TABLE IF EXISTS %s" % config.database_table_name)
    cursor.close()
    connection.close()


class TestMain(TestCase):

    def check_non_empty_database(self, result=True):
        connection = DatabaseHandler.get_connection()
        cursor = connection.cursor()
        cursor.execute("SELECT count(*) FROM %s" % config.database_table_name)
        if result:
            self.assertGreater(cursor.fetchone()[0], 0)
        else:
            self.assertEqual(cursor.fetchone()[0], 0)
        connection.close()

    def clear_and_check(self, fun, arg, result=True):
        clear_data()
        fun(arg)
        self.check_non_empty_database(result)

    def test_main(self):
        # Test arguments
        collecting_data.main(None)
        collecting_data.main((None, None))
        collecting_data.main((None, None, None))
        collecting_data.main(('', ''))
        collecting_data.main(('', '2'))
        collecting_data.main(('', 'das'))
        collecting_data.main(('', '12,12'))
        collecting_data.main(('', '12.48'))
        collecting_data.main(('', '2548-98562/84'))
        collecting_data.main(('', '12/5.2017'))

        # Test date parsing
        for network in AdNetworkHandler.supported_networks:
            collecting_data.main((network, '12'))
            self.clear_and_check(collecting_data.main, (network, '15.9/2017'), False)
            self.clear_and_check(collecting_data.main, (network, '2017.9/15'), False)

            self.clear_and_check(collecting_data.main, (network, '2017/9/15'))
            self.clear_and_check(collecting_data.main, (network, '2017.9.15'))
            self.clear_and_check(collecting_data.main, (network, '2017-9-15'))
            
            self.clear_and_check(collecting_data.main, (network, '15/9/2017'))
            self.clear_and_check(collecting_data.main, (network, '15.9.2017'))
            self.clear_and_check(collecting_data.main, (network, '15-9-2017'))

            self.clear_and_check(collecting_data.main, (network, '15/09/2017'))
            self.clear_and_check(collecting_data.main, (network, '15.09.2017'))
            self.clear_and_check(collecting_data.main, (network, '15-09-2017'))

            self.clear_and_check(collecting_data.main, (network, '2017/9/1', True))
            self.clear_and_check(collecting_data.main, (network, '2017.9.1', True))
            self.clear_and_check(collecting_data.main, (network, '2017-9-1', True))

            self.clear_and_check(collecting_data.main, (network, '2017/9/01', True))
            self.clear_and_check(collecting_data.main, (network, '2017.9.01', True))
            self.clear_and_check(collecting_data.main, (network, '2017-9-01', True))
    
            self.clear_and_check(collecting_data.main, (network, '1/9/2017', True))
            self.clear_and_check(collecting_data.main, (network, '1.9.2017', True))
            self.clear_and_check(collecting_data.main, (network, '1-9-2017', True))

            self.clear_and_check(collecting_data.main, (network, '01/09/2017', True))
            self.clear_and_check(collecting_data.main, (network, '01.09.2017', True))
            self.clear_and_check(collecting_data.main, (network, '01-09-2017', True))

        # Check if table is correct
        self.connection = DatabaseHandler.get_connection()
        cursor = self.connection.cursor()

        # Get table structure
        cursor.execute("PRAGMA table_info(%s);" % config.database_table_name)
        result = cursor.fetchall()

        # Compare table structure with expected
        self.assertEqual(len(result),
                         len(TestDatabaseHandler.REQUIRED_TABLE_STRUCTURE), 'Columns length does not match')
        for res, expected in zip(result, TestDatabaseHandler.REQUIRED_TABLE_STRUCTURE):
            self.assertEqual(res[1], expected[0], 'Column name does not match')
            self.assertTrue(res[2].__contains__(expected[1]), 'Column type does not match')
            self.assertEqual(res[3], expected[2], "Column 'Not null' does not match")
            self.assertEqual(res[4], expected[3], "Column default value does not match")
            self.assertEqual(res[5], expected[4], "Column 'prime key' does not match")
        self.connection.close()
