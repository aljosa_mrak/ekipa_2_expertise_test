cimport numpy as np
import datetime
import numpy as np

cdef float _parse_revenue_super_network(str x):
    return float(x[3:])

cdef np.ndarray _parse_revenue_umbrella_network(np.ndarray data, converter):
    cdef np.ndarray result = np.empty(data.shape[0])
    cdef date

    for i in range(data.shape[0]):
        date = datetime.date(data[i,0].year, data[i,0].month, data[i,0].day)
        if converter.bounds['USD'].last_date < date:
            result[i] = converter.convert(float(data[i,1]), 'USD', 'EUR')
        else:
            result[i] = converter.convert(float(data[i,1]), 'USD', 'EUR', date=date)

    return result


def parse_revenue_super_network(str x):
    return _parse_revenue_super_network(x)

def parse_revenue_umbrella_network(np.ndarray data, converter):
    return _parse_revenue_umbrella_network(data, converter)


