import os
import subprocess
import sys
import time

import pandas as pd
import numpy as np


class Logger(object):
    def __init__(self, script_name, to_file=False, verbose=True):
        self.to_file = to_file
        self.verbose = verbose

        self.terminal = sys.stdout
        if self.to_file:
            if not os.path.exists('log/'):
                os.makedirs('log/')
            self.log = open('log/log_' + time.strftime("%Y%m%d-%H%M%S") + '.txt', 'w', 1)

        # Create header
        self.write('Script running: ' + os.path.basename(sys.argv[0]) + '\n')
        self.write('Run time: ' + time.strftime("%Y %m %d - %H:%M:%S") + '\n')
        try:
            self.write(
                'Git repository version: ' + str(subprocess.check_output(['git', 'rev-parse', 'HEAD'])[:-1]) + '\n\n')
        except Exception:
            self.write('Not a git repository\n\n')

        # Print library versions
        self.write('Python version ' + sys.version + '\n')
        self.write('Numpy version ' + np.__version__ + '\n')
        self.write('Pandas version ' + pd.__version__ + '\n\n')

    def write(self, message):
        if not self.verbose:
            return

        self.terminal.write(message)
        if self.to_file:
            self.log.write(message.replace('\033[1m', '').replace("\033[0m", ""))

    def flush(self):
        self.terminal.flush()
        if self.to_file:
            self.log.flush()
