from unittest import TestCase

from AdNetworks import AdNetworkHandler


class TestAdNetworkHandler(TestCase):

    def test_get_report(self):
        # Test unsupported network
        self.assertIsNone(AdNetworkHandler.get_report(None, -1, -1, -1))
        self.assertIsNone(AdNetworkHandler.get_report("", -1, -1, -1))
        self.assertIsNone(AdNetworkHandler.get_report("Unsupported", -1, -1, -1))

        for network in AdNetworkHandler.supported_networks:
            # Test file does not exist
            self.assertIsNone(AdNetworkHandler.get_report(network, -1, -1, -1))

            self.assertIsNotNone(AdNetworkHandler.get_report(network, 15, 9, 2017))
            self.assertIsNotNone(AdNetworkHandler.get_report(network, 16, 9, 2017))

            self.assertIsNotNone(AdNetworkHandler.get_report(network, 15, 9, 2017, from_file=False))
            self.assertIsNotNone(AdNetworkHandler.get_report(network, 16, 9, 2017, from_file=False))
