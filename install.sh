#!/bin/bash

pip install currencyconverter
pip install requests
pip install pandas==0.19.2
pip install dateutils
pip install numpy
pip install -U matplotlib
pip install scipy

python setup.py build_ext --inplace