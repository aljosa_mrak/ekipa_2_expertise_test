import functools
import sys
import timeit

from helper.Logger import Logger


def run(run_fun, args, to_file, verbose):
    # Run stout through Logger
    sys.stdout = Logger(__file__, to_file=to_file, verbose=verbose)

    # Time function
    run_time = timeit.timeit(functools.partial(run_fun, args), number=1)

    # Print result
    print("\n\nElapsed time: %.2f min" % (run_time / 60))
