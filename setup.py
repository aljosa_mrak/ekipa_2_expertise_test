from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("cyton", ["helper/cyton.pyx"]),
]

setup(
    include_dirs='.',
    name='app',
    cmdclass={'build_ext': build_ext},
    ext_modules=ext_modules, requires=['requests', 'pandas', 'currencyconverter', 'dateutils', 'numpy', 'matplotlib',
                                       'scipy'],
    install_requires=['requests', 'pandas', 'currencyconverter', 'dateutils', 'numpy', 'matplotlib', 'scipy'],
)
