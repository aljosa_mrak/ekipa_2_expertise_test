from unittest import TestCase

import os

import config
from Database import DatabaseHandler


class TestDatabaseHandler(TestCase):

    REQUIRED_TABLE_STRUCTURE = [('id', 'INTEGER', 1, None, 1),
                                ('date', 'DATE', 0, None, 0),
                                ('adNetwork', 'CHAR', 0, None, 0),
                                ('app', 'CHAR', 0, None, 0),
                                ('platform', 'CHAR', 0, None, 0),
                                ('requests', 'INTEGER', 0, None, 0),
                                ('impressions', 'INTEGER', 0, None, 0),
                                ('revenue', 'DECIMAL', 0, None, 0)]

    def tearDown(self):
        if self.connection is not None:
            self.connection.rollback()
            self.connection.close()

    def test_get_connection_no_database(self):
        if config.database_type == 'sqlite':
            if os.path.exists(config.sqlite_file_name):
                os.remove(config.sqlite_file_name)
            self.connection = DatabaseHandler.get_connection()
            self.assertTrue(os.path.exists(config.sqlite_file_name),
                            "Path '%s' does not exist" % config.sqlite_file_name)
            self.assertTrue(self.table_exists(), 'Database does not contable table: ' + config.database_table_name)

    def test_get_connection_no_folder(self):
        if config.database_type == 'sqlite':
            dir_list = os.listdir(os.path.dirname(config.sqlite_file_name))
            if len(dir_list) == 0 or (len(dir_list) == 1 and dir_list[0] == os.path.basename(config.sqlite_file_name)):
                if os.path.exists(config.sqlite_file_name):
                    os.remove(config.sqlite_file_name)
                if os.path.exists(os.path.dirname(config.sqlite_file_name)):
                    os.removedirs(os.path.dirname(config.sqlite_file_name))
                self.connection = DatabaseHandler.get_connection()
                self.assertTrue(os.path.exists(config.sqlite_file_name),
                                "Path '%s' does not exist" % config.sqlite_file_name)
                self.assertTrue(self.table_exists(), 'Database does not contable table: ' + config.database_table_name)

    def test_create_table(self):
        self.connection = DatabaseHandler.get_connection()
        cursor = self.connection.cursor()

        # Test invalid parameters
        self.assertFalse(DatabaseHandler.create_table(None, None))
        self.assertFalse(DatabaseHandler.create_table(None, 'test'))
        self.assertFalse(DatabaseHandler.create_table(self.connection, None))
        self.assertFalse(DatabaseHandler.create_table(self.connection, ''))
        self.assertFalse(DatabaseHandler.create_table(self.connection, '0'))

        self.assertTrue(DatabaseHandler.create_table(self.connection, 'a'))
        cursor.execute("DROP TABLE IF EXISTS a")

        # Test correct creating
        cursor.execute("DROP TABLE IF EXISTS %s" % config.database_table_name)
        self.assertTrue(DatabaseHandler.create_table(self.connection, config.database_table_name))

        # Check if table exists
        self.assertTrue(self.table_exists(), 'Database does not contain table: ' + config.database_table_name)

        # Get table structure
        cursor.execute("PRAGMA table_info(%s);" % config.database_table_name)
        result = cursor.fetchall()

        # Compare table structure with expected
        self.assertEqual(len(result), len(self.REQUIRED_TABLE_STRUCTURE), 'Columns length does not match')
        for res, expected in zip(result, self.REQUIRED_TABLE_STRUCTURE):
            self.assertEqual(res[1], expected[0], 'Column name does not match')
            self.assertTrue(res[2].__contains__(expected[1]), 'Column type does not match')
            self.assertEqual(res[3], expected[2], "Column 'Not null' does not match")
            self.assertEqual(res[4], expected[3], "Column default value does not match")
            self.assertEqual(res[5], expected[4], "Column 'prime key' does not match")

    def table_exists(self):
        cursor = self.connection.cursor()
        cursor.execute(
            """SELECT name FROM sqlite_master WHERE type='table' AND name='%s';""" % config.database_table_name)
        result = cursor.fetchall()
        return len(result) == 1 and result[0][0] == config.database_table_name
