import pandas as pd

from AdNetworks.AbstractAdNetwork import *
from helper.cyton import parse_revenue_super_network


class SuperNetwork(AbstractAdNetwork):

    name = 'SuperNetwork'
    url_pattern = 'https://storage.googleapis.com/expertise-test/supernetwork/report/daily/%d-%02d-%02d.csv'
    file_name_pattern = 'data/%d-%02d-%02d.csv'

    def get_url(self, day, month, year):
        return self.url_pattern % (year, month, day)

    def get_file_name(self, day, month, year):
        return self.file_name_pattern % (year, month, day)

    def read_csv(self, source):
        return pd.read_csv(source,
                           sep=",",
                           header=0,
                           names=COLUMN_NAMES,
                           engine='c',
                           dtype={APP: 'str', PLATFORM: 'str', REQUESTS: 'int', IMPRESSIONS: 'int'},
                           converters={REVENUE: parse_revenue_super_network},
                           parse_dates=[DATE])
