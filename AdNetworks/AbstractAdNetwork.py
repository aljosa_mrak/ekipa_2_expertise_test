DATE = 'date'
AD_NETWORK = 'adNetwork'
APP = 'app'
PLATFORM = 'platform'
REQUESTS = 'requests'
IMPRESSIONS = 'impressions'
REVENUE = 'revenue'
COLUMN_NAMES = [DATE, APP, PLATFORM, REQUESTS, IMPRESSIONS, REVENUE]


class AbstractAdNetwork(object):
    name = None
    url_pattern = None
    file_name_pattern = None
    df = None

    def get_url(self, day, month, year):
        raise NotImplementedError("Should have implemented this")

    def get_file_name(self, day, month, year):
        raise NotImplementedError("Should have implemented this")

    def read_csv(self, source):
        raise NotImplementedError("Should have implemented this")

    def load_data(self, source):
        if source is None:
            print 'Source is None'
            return

        try:
            self.df = self.read_csv(source)
            self.df[AD_NETWORK] = self.name
        except IOError as e:
            print e
        except Exception as e:
            print 'Problem with parsing. Check if file format is correct'
            print e
