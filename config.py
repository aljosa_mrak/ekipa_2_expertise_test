#!/usr/bin/env python

# Database type to use. Each type must be implemented. Currently supported sqlite
database_type = 'sqlite'

# Database table name
database_table_name = 'daily_report'

# Sqlite database file name
sqlite_file_name = 'database/database.db'
