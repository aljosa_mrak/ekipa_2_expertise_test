import glob
import importlib
import inspect
import io
import os
import requests

from AdNetworks.AbstractAdNetwork import AbstractAdNetwork

supported_networks = dict()

#################################
# Load all implemented networks #
#################################

current_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)))
current_module_name = os.path.splitext(os.path.basename(current_dir))[0]
tmp = glob.glob(current_dir + "/*.py")
for _file in glob.glob(current_dir + "/*.py"):
    name = os.path.splitext(os.path.basename(_file))[0]

    # Ignore __ files
    if name.startswith("__"):
        continue

    _module = importlib.import_module("." + name, package='AdNetworks')

    for member in dir(_module):
        handler_class = getattr(_module, member)

        # Add only those who extend AbstractAdNetwork
        if handler_class and inspect.isclass(handler_class) and inspect.getmro(handler_class)[1] == AbstractAdNetwork:
            supported_networks[handler_class.name] = handler_class


def get_report(ad_network, day, month, year, from_file=True):
    # Check if ad_network is supported
    if ad_network not in supported_networks:
        print 'Network not supported'
        print 'Supported networks are', ', '.join(supported_networks)
        return None

    # Get specific network handler class
    handler_class = supported_networks[ad_network]()

    # Get data from file or download it
    if from_file:
        # Create file location from date
        source = handler_class.get_file_name(day, month, year)
        if not os.path.isfile(source):
            print "File '%s' does not exist" % source
            return None

        print "Using file '%s'" % source
    else:
        # Create url location from date
        url = handler_class.get_url(day, month, year)

        response = requests.get(url)
        if not response.ok:
            print "Unable to get data from '%s', status code: %d, reason:%s" %\
                  (url, response.status_code, response.reason)
            return None

        source = io.StringIO(response.content.decode('utf-8'))
        print "Using url '%s'" % url


    # Load data
    handler_class.load_data(source)

    return handler_class