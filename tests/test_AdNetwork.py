from unittest import TestCase

import os

from AdNetworks import AdNetworkHandler
from AdNetworks.SuperNetwork import SuperNetwork
from AdNetworks.UmbrellaNetwork import UmbrellaNetwork
from tests.test_DatabaseHandler import TestDatabaseHandler


class TestAdNetwork(TestCase):

    def test_get_file_name(self):
        self.assertTrue(os.path.exists(SuperNetwork().get_file_name(15, 9, 2017)))
        self.assertTrue(os.path.exists(SuperNetwork().get_file_name(16, 9, 2017)))

        self.assertTrue(os.path.exists(UmbrellaNetwork().get_file_name(15, 9, 2017)))
        self.assertTrue(os.path.exists(UmbrellaNetwork().get_file_name(16, 9, 2017)))

    def test_load_data_file(self):
        for network in AdNetworkHandler.supported_networks.values():
            # Test not correct file
            network().load_data(None)
            network().load_data('does_not_exist.file')
            network().load_data('.')
            network().load_data('config.py')

            network_instance = network()
            network_instance.load_data(network_instance.get_file_name(16, 9, 2017))
            self.assertIsNotNone(network_instance.df)

            # Test columns
            col = network_instance.df.columns
            self.assertIsNotNone(col)
            self.assertEqual(len(col), len(TestDatabaseHandler.REQUIRED_TABLE_STRUCTURE[1:]))
            self.assertListEqual(sorted([e for e in col]),
                                 sorted([e[0] for e in TestDatabaseHandler.REQUIRED_TABLE_STRUCTURE[1:]]))

    def test_load_data_url(self):
        for network in AdNetworkHandler.supported_networks.values():
            network_instance = network()
            network_instance.load_data(network_instance.get_url(16, 9, 2017))
            self.assertIsNotNone(network_instance.df)

            # Test columns
            col = network_instance.df.columns
            self.assertIsNotNone(col)
            self.assertEqual(len(col), len(TestDatabaseHandler.REQUIRED_TABLE_STRUCTURE[1:]))
            self.assertListEqual(sorted([e for e in col]),
                                 sorted([e[0] for e in TestDatabaseHandler.REQUIRED_TABLE_STRUCTURE[1:]]))
