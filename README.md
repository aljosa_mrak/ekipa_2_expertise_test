# Ekipa 2 Expertise test
This is my python solution for the expertise test for the job application at Ekipa 2. Problem description is provided in [Problem details](Expertise test - backend data software engineer 2 - ekipa2.pdf)

## Install dependencies
This project is using Python 2.7
To install all python library dependencies run the install script, which will install them using **pip**.
```sh
sudo ./install.sh
```

## Problem 1: Collecting data

### Assumptions
In my solution I have made these assumptions:
- File location for SuperNetwork has leading zeros for day and month while AdUmbrella does not
- All revenue is converted and stored in €. The conversion is using the date of that row
- There is no system for checking duplicate data

### Running
The application takes two arguments. The first one is the ad network name and the second is the date.

Currently only **AdUmbrella** **SuperNetwork** networks are supported

Date parameter can be in the following formats: **2017/9/15**, **2017.9.15**, **2017-9-15**, **15/09/2017**, **15.9.2017**, **15-09-2017**, Where 2017 is the year, 9 is the month and 15 in the day.

There is an optional flag parameter **-f** to read data from a file located in subfolder **data/**.

The program is run using the following commands
```sh
python collecting_data.py AdUmbrella 2017-09-15
python collecting_data.py SuperNetwork 2017.09.15 -f
```

### Ad Network implementations
Ad Network implementations are located in [AdNetworks](AdNetworks). Each Implementation is a new file, it must extend **AbstractAdNetwork** and implement methods ** get_url**, **get_file_name** and **read_csv**. Examples can be seen in [SuperNetwork.py](AdNetworks/SuperNetwork.py) and [UmbrellaNetwork.py](AdNetworks/UmbrellaNetwork.py).

### Database
Currently database is implemented using **SQLite** in file **database/database.db**. Other implementation can be done by adding new connection code in [DatabaseHandler.py](Database/DatabaseHandler.py)

### Configurations
Project configuration are located in [config.py](config.py). It contains database configurations

### Unit testing
Unit test are in [Tests](tests) and can be run using PyCharm.
```sh
python $PYCHARM_HOME/helpers/pycharm/_jb_unittest_runner.py --path tests
```

### Sanity check
[sanity_check.py](sanity_check.py) compares different files from different adNetworks and statistically compares them. It prints the statistical possibility of a specific file being a statistical outlier.

Example of the output
```
File '2017-09-16.csv' does not look OK with probability of 94.83 %
File '2017-09-15.csv' does not look OK with probability of 44.21 %
File 'adumbrella-16_9_2017.csv' does not look OK with probability of 28.39 %
File 'adumbrella-15_9_2017.csv' does not look OK with probability of 26.80 %

```

The script can be run using the following command

```sh
python sanity_check.py data
```

Usage
```sh
python sanity_check.py folder_1 folder_2 ...
```